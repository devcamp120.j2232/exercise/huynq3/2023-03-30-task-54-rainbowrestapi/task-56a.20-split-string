package com.devcamp.splitstring.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResSplitString {
    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> splitString() {
        String inputString= "Xin Chào Tất Cả Các Bạn VIỆT NAM";
        String[] words = inputString.split("\\s+");
        ArrayList<String> result = new ArrayList<>();
        for (String word : words) {
            result.add(word);
        }
        return result;
    }
}
